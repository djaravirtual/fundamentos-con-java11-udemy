package com.udemy.programacion.excepciones;

public class OperationException extends Exception {

    public OperationException(String mensaje) {
        super(mensaje);
    }
}
