package com.udemy.programacion.enumeraciones;

// Lista de valores que son constantes
public enum Dias {
    LUNES,
    MARTES,
    MIERCOLES,
    JUEVES,
    VIERNES,
    SABADO,
    DOMINGO
}
